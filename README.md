# Minio Dashboard 

This dashboard is designed to visualize MinIO metrics by Grafana using influxdb as database.
To use this dashboard, you need to enable monitoring in MinIO and create influxdb scraper to collect metrics from MinIO cluster. You can follow Minio official documents to do that:

https://min.io/docs/minio/linux/operations/monitoring/monitor-and-alert-using-influxdb.html


Then just inport import this json file in you grafana and choose your influxdb as datasource and you'll be able to see most important metrics in a resonable way.

##influxdb
##grafana
##MinIO
